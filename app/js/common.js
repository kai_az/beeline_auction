$(document).ready(function () {
    $('.languages').click(function (e) {
        e.preventDefault();
        var ch = $('.languages a');
        ch.each( function () {
            $(this).toggleClass('active');
        })
    });

    //paroller parallax

    $('.infoblock').paroller(
        {
            factor: 0.5,
            factorXs: 0.2,
            direction: 'vertical'
        }
    );

    $('.main-cabinet').paroller(
        {
            factor: 0.5,
            factorXs: 0.2,
            direction: 'vertical'
        }
    );

    // flipclock

    // var clock = $('.clock').FlipClock(3600 * 24 * 3, {
    //     clockFace: 'DailyCounter',
    //     countdown: true
    // });

    //number slider

    $('.number-slier-3').on('init', function(event, slick){
        var slc = $('.slick-list');
        slc.each(function (index, elem) {
            $(elem).wrap('<div class="list-wrap"></div>')
        });

        var slcwrap = $('.list-wrap');
        slcwrap.each(function (index, elem) {
            $(elem).append('<div class="list-wrap-gradient"></div>')
        });

        // $('.number-slier-3 .slick-current').next().css(            {
        //         'font-size': '45px',
        //     }
        // );


    });
    // $(this).on('afterChange', function(event, slick, currentSlide, nextSlide){
    //     $('.number-slier-3 [data-slick-index=' + currentSlide + ']').next().css(
    //         {
    //             'font-size': '45px',
    //         }
    //         );
    //     $('.number-slier-3 [data-slick-index=' + currentSlide + ']').css('font-size', '31px');
    //
    // });


    $('.number-slier-1').slick({
        slide: '.item',
        vertical: true,
        slidesToShow: 3,
        arrows: true,
        verticalSwiping: true,
        rows: 0,
        // centerMode: true,
        centerPadding: '0px',
        centerMode: true
    });

    $('.number-slier-2').slick({
        vertical: true,
        slidesToShow: 3,
        arrows: true,
        verticalSwiping: true,
        centerMode: true,
        centerPadding: '0px'

    });

    $('.number-slier-3').slick({
        vertical: true,
        slidesToShow: 3,
        arrows: true,
        verticalSwiping: true,
        centerMode: true,
        centerPadding: '0px',
        infinite: true,

    });


    // $('.number-slier-3').on('afterChange', function(event, slick, currentSlide){
    //     if (currentSlide == 5) { console.log('Осуществлён переход к 5му слайду'); }
    // });
    //
    // var csl = $('.number-slier-3').slick('slickCurrentSlide');
    // console.log(csl);
    //
    // $('.number-slier-3').on('afterChange', function(event, slick, currentSlide, nextSlide){
    //     console.log(currentSlide);
    // });










});